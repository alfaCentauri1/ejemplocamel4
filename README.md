# EjemploCamel4
Este ejemplo muestra cómo Camel es capaz de cargar rutas durante el inicio utilizando el nuevo sistema de carga de rutas. 
El cargador de rutas tiene soporte para cargar rutas en XML, Java y YAML (se agregarán otros lenguajes).

En este ejemplo, el enfoque está en cómo puede usar la configuración de rutas globales con el DSL para separar el manejo 
de errores (y otras funciones de rutas cruzadas) de todas sus rutas.

Este ejemplo tiene una ruta en Java, XML y YAML. Cada una de esas rutas hace referencia a una configuración de ruta 
específica, que también está _codificada_ en el mismo idioma que la ruta. Pero esto no es necesario, puede usar Java 
para codificar sus configuraciones de ruta para el manejo avanzado de errores y luego _codificar_ sus rutas en otros 
lenguajes como XML o YAML.

## Requisitos
* JDK 1.8.
* Maven.
* 

## Instalación
* Compile el proyecto con el IDE de su preferencia.
* Ingrese por consola a la raíz del proyecto.
* Ejecute en la consola **java -jar target/ejemplocamel4.jar**

## Authors and acknowledgment
[GitLab: alfaCentauri1](https://gitlab.com/alfaCentauri1)

## License
GNU version 3.

## Project status
* Developer

### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.